Pickin-Open-Sticks
==================

A group Pickin' Sticks project developed by the Moosader community and looking for **additional contributions**!

## Contributing to the Project

### Why you should contribute
If you're bored and want to work on something open source, mainly.
There's a working game here, and you can add features!

### How to Contribute
Fork this repo!  If we like your changes, we can add it to the core game!

### What should I work on?
If you cannot think of a feature to add, or if you've thought of something
but you don't want to write it right now, check the **Issues page**!
https://github.com/Moosader/Pickin-Open-Sticks/issues?state=open

Look at the available tasks, or create a task, that could be done with the game!


------------------------------------------------------------


## Original Repo
The original repo is hosted on Google Code, here:
http://code.google.com/p/pickin-open-sticks/


------------------------------------------------------------


## Contributors

If you contributed to this project, please let me know what your website, blog, etc. is
so I can add a proper link to you.

### Rachel J. Morris
http://www.github.com/moosader
http://www.moosader.com/

### Sutabi
http://code.google.com/u/105113917365398481197/

### Lotios611
http://code.google.com/u/117169036249098462728/

### Shane

### Eric Mitchem





