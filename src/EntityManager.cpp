/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "EntityManager.h"

void EntityManager::InitPosAssets( pe::ImageManager& imgMgr, pe::System& system )
{
    Character player( imgMgr.GetAsset("player").GetImage() );
    AddCharacter( "player", player );

    Item stick( imgMgr.GetAsset("stick").GetImage() );
    stick.SetRange( system.GetScreenWidth(), system.GetScreenHeight() );
    stick.GenerateNewCoordinates();
    AddItem( "stick", stick );

    // Left barrier
    Item barrier;
    barrier.SetCoords( -32, -32, 32, system.GetScreenHeight() + 32 );
    AddItem( "scrBarrierLeft", barrier );
    // Right barrier
    barrier.SetCoords( system.GetScreenWidth(), -32, system.GetScreenWidth() + 32, system.GetScreenHeight() + 32 );
    AddItem( "scrBarrierRight", barrier );
    // Top barrier
    barrier.SetCoords( -32, -32, system.GetScreenWidth() + 32, 32 );
    AddItem( "scrBarrierTop", barrier );
    // Bottom barrier
    barrier.SetCoords( -32, system.GetScreenHeight(), system.GetScreenWidth() + 32, system.GetScreenHeight() + 32 );
    AddItem( "scrBarrierBottom", barrier );
}

void EntityManager::Update( pe::SoundManager& sndMgr )
{
    // TODO: Iterator
    if ( m_characters["player"].CollisionWith( &m_items["stick"] ) )
    {
        m_items["stick"].GenerateNewCoordinates();
        m_characters["player"].IncrementScore();
        sndMgr.PlaySound("pick");
        std::cout << m_characters["player"].GetScoreString() << std::endl;
    }

    if ( m_characters["player"].CollisionWith( &m_items["scrBarrierLeft"] ) )
    {
        m_characters["player"].Move( RIGHT );
    }
    else if ( m_characters["player"].CollisionWith( &m_items["scrBarrierRight"] ) )
    {
        m_characters["player"].Move( LEFT );
    }

    if ( m_characters["player"].CollisionWith( &m_items["scrBarrierTop"] ) )
    {
        m_characters["player"].Move( DOWN );
    }
    else if ( m_characters["player"].CollisionWith( &m_items["scrBarrierBottom"] ) )
    {
        m_characters["player"].Move( UP );
    }
}

Character& EntityManager::GetCharacter( const std::string& name )
{
    return m_characters[ name ];
}

Item& EntityManager::GetItem( const std::string& name )
{
    return m_items[ name ];
}

void EntityManager::AddCharacter( std::string name, Character& item )
{
    m_characters.AddAsset( name, item );
}

void EntityManager::AddItem( std::string name, Item& item )
{
    m_items.AddAsset( name, item );
}
