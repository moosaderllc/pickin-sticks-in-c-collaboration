/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#ifndef _CHARACTER
#define _CHARACTER

#include <sstream>

#include "PosEngine/Entity.h"
#include "PosEngine/Animation.h"


// The direction #'s are based on the spritesheet layout

enum Direction {
    DOWN = 0, UP = 1, LEFT = 2, RIGHT = 3
};

/**
 * Character class - represents objects such as the player or npcs
 * @author Rachel Morris
 */
class Character : public pe::Entity {
protected:
    pe::Animation m_anim; // added by luke
    float m_walkSpeed;
    int m_score;
public:
    Character() { ; }
    Character(sf::Image&);
    void Initialize();

    void Move(Direction dir);
    void SetSpeed(float val);
    void IncrementScore();
    int GetScore();

    std::string GetScoreString();
    std::string GetRank();
};

#endif
