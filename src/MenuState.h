/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "PosEngine/State.h"

#include "PosEngine/SoundManager.h"
#include "PosEngine/Rectangle.h"
#include "PosEngine/ImageManager.h"
#include "PosEngine/System.h"
#include "PosEngine/Map.h"
#include "Item.h"
#include "Character.h"

#ifndef _MenuState
#define _MenuState

// TODO: Write.
class MenuState : public pe::StateInterface
{
    public:
        MenuState( std::string name, pe::System* system );
        virtual void Init();
        virtual ProgramState MainLoop();
};

#endif
