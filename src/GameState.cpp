/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "GameState.h"

GameState::GameState( std::string name, pe::System* system )
{
    m_system = system;
    m_name = name;

    Init();
}

void GameState::Init()
{
    // TODO: Remove singletons
    srand(time(0));
    m_imageMgr.Init();
    m_map.Initialize(m_imageMgr.GetAsset( "grass" ).GetImage());
    m_entityMgr.InitPosAssets( m_imageMgr, *m_system );
}

ProgramState GameState::MainLoop()
{
    sf::Clock fpsClock;
    while ( m_system->GetState() == RUNNING )
    {
        m_system->CheckEvents();
        m_system->HandleKeyboardInput( m_entityMgr.GetCharacter( "player" ) );

        m_entityMgr.Update( m_soundMgr );

        // Draw to screen
        m_system->BeginDraw();
        m_map.Render((*m_system));
        m_system->Draw( m_entityMgr.GetCharacter( "player" ).GetSprite() );
        m_system->Draw( m_entityMgr.GetItem( "stick" ).GetSprite() );

        // Render FPS
        if (fpsClock.GetElapsedTime() > 1.0) {
            std::cout << "FPS: " << m_system->GetFramesPerSecond() << std::endl;
            fpsClock.Reset();
        }

        m_system->EndDraw();
    } // while ( system.GetState() == RUNNING )

    return m_system->GetState();
}
