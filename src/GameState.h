/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "PosEngine/State.h"

#include "PosEngine/SoundManager.h"
#include "PosEngine/Rectangle.h"
#include "PosEngine/ImageManager.h"
#include "PosEngine/System.h"
#include "PosEngine/Map.h"
#include "Item.h"
#include "Character.h"
#include "EntityManager.h"

#ifndef _GameState
#define _GameState

class GameState : public pe::StateInterface
{
    public:
        GameState( std::string name, pe::System* system );
        virtual void Init();
        virtual ProgramState MainLoop();
    private:
        EntityManager m_entityMgr;
        pe::ImageManager m_imageMgr;
        pe::SoundManager m_soundMgr;
        pe::Map::Map m_map;

        bool Collision(const pe::Rectangle& A, const pe::Rectangle& B); // TODO: Remove - this is temporary
};

#endif
