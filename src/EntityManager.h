/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "PosEngine/ManagerTemplate.h"
#include "PosEngine/Entity.h"
#include "PosEngine/ImageManager.h"
#include "PosEngine/SoundManager.h"
#include "PosEngine/System.h"

#include "Character.h"
#include "Item.h"

#ifndef _EntityManager
#define _EntityManager

class EntityManager
{
    public:
        Character& GetCharacter( const std::string& name );
        Item& GetItem( const std::string& name );
        void AddCharacter( std::string name, Character& item );
        void AddItem( std::string name, Item& item );
        void InitPosAssets( pe::ImageManager& imgMgr, pe::System& system ); // Kind of cheating - bad because it creates coupling between unrelated managers
        void Update( pe::SoundManager& sndMgr ); // TODO: Fix haxxy manager passing

    private:
        pe::ManagerTemplate<Character> m_characters;
        pe::ManagerTemplate<Item> m_items;
};

#endif
