/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include <string>
#include "Include.h"
#include "PosEngine/StateManager.h"
#include "GameState.h"
#include "MenuState.h"

int main()
{
    pe::System system;
    system.SetState(RUNNING);

    pe::StateManager stateManager;
    GameState gameState( "game", &system );
    stateManager.AddAsset( "game", &gameState );

    stateManager.SetActiveState( "game" );

    ProgramState programState;
    while ( programState != EXIT )
    {
        programState = stateManager.MainLoop();

        std::cout << "Returned state was " << programState << std::endl;

        if ( programState == MENU )
        {
            break;
        }
        else if ( programState == RUNNING )
        {
            stateManager.SetActiveState( "game" );
        }
    }

    return 0;
} // int main()
