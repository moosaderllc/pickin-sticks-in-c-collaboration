/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "Character.h"

Character::Character(sf::Image& image) {
    Initialize();
    SetImage(image);
}

void Character::Initialize() {
    m_coord.Setup(0, 0, 32, 48);
    m_walkSpeed = 3.0f;
    m_anim.Initialize(4, 4, 0.18f);
    m_score = 0;
}

/**
 * Character::Move
 * Moves the character based on the direction given
 */
void Character::Move(Direction dir) {
    if (dir == UP) {
        m_anim.ChangeAnim(UP);
        m_coord.y -= m_walkSpeed;
    } else if (dir == DOWN) {
        m_anim.ChangeAnim(DOWN);
        m_coord.y += m_walkSpeed;
    } else if (dir == LEFT) {
        m_anim.ChangeAnim(LEFT);
        m_coord.x -= m_walkSpeed;
    } else if (dir == RIGHT) {
        m_anim.ChangeAnim(RIGHT);
        m_coord.x += m_walkSpeed;
    }
    m_anim.Animate(m_sprite);
}

std::string Character::GetScoreString() {
    std::stringstream txtScore;
    txtScore << "Score: " << m_score;

    return txtScore.str();
}

std::string Character::GetRank() {
    std::string rank;
    if (m_score < 10) {
        rank = "Newbie";
    } else if (m_score < 20) {
        rank = "Twig Sharpener";
    } else if (m_score < 30) {
        rank = "Toothpick Whittler";
    } else if (m_score < 40) {
        rank = "Popsickle Stick Apprentice";
    } else if (m_score < 50) {
        rank = "Bonsai Samurai";
    } else if (m_score < 75) {
        rank = "Stump Sorter";
    } else if (m_score < 100) {
        rank = "Plank Patron";
    } else if (m_score < 125) {
        rank = "Tree Wannabe";
    } else if (m_score < 150) {
        rank = "Elder Oak";
    } else {
        rank = "Yggdrasil";
    }
    return rank;
}

void Character::SetSpeed(float val) {
    m_walkSpeed = val;
}

void Character::IncrementScore() {
    m_score++;
}

int Character::GetScore() {
    return m_score;
}

