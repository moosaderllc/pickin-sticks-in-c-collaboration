/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "MenuState.h"

MenuState::MenuState( std::string name, pe::System* system )
{
    m_system = system;
    m_name = name;
}

void MenuState::Init()
{
}

ProgramState MenuState::MainLoop()
{
    // TEMP: Moved everything out of main() to here
//    ImageManager& imgMgr = ImageManager::GetSingleton();
//    imgMgr.Initialize();
//    SoundManager sndMgr;
//    Character player(imgMgr.GetImage("player"));
//    Item stick(imgMgr.GetImage("default"));
//
//    srand(time(0));
//
//    stick.SetRange(m_system->GetScreenWidth(), m_system->GetScreenHeight());
//    stick.Collect(); // sets it to a random spot
//    PickinOpenSticks::Map::Map map;
//    map.Initialize(imgMgr.GetImage(3));
//
//    // Setup a clock for fps display
//    sf::Clock fpsClock;
//
//    while (m_system->GetState() != EXIT) {
//
//        switch (m_system->GetState()) {
//            case MENU:
//                m_system->CheckEvents();
//                //draw menu
//            {
//                m_system->BeginDraw();
//                m_system->EndDraw();
//            }
//                break;
//            case RUNNING:
//            {
//                m_system->CheckEvents();
//                m_system->HandleKeyboardInput(player);
//
//                // Draw to screen
//                m_system->BeginDraw();
//                map.Render((*m_system));
//                m_system->Draw(player.GetSprite());
//                m_system->Draw(stick.GetSprite());
//
//                // Render FPS
//                if (fpsClock.GetElapsedTime() > 1.0) {
//                    std::cout << "FPS: " << m_system->GetFramesPerSecond() << std::endl;
//                    std::cout << "Menu State" << std::endl;
//                    fpsClock.Reset();
//                }
//
//                m_system->EndDraw();
//                break;
//            }
//            case PAUSED:
//            default:
//                break;
//        }
//    } // while ( system.GetState() == RUNNING )

    return EXIT;
}
