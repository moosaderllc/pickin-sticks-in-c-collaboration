/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#ifndef STICK_H
#define STICK_H

#include "stdio.h"
#include "PosEngine/Entity.h"

/**
 *  @author Luke Fangel, Rachel Morris
 *  @class Item
 *
 *  Generic item class, for things like sticks and powerups.
 *  These have less functionality than a Character.
 * Both are derived from Entity, base object class.
 **/
class Item : public pe::Entity {
public:
    Item() { ; }
    Item(sf::Image&);
    void Initialize();
    void GenerateNewCoordinates();
    void SetRange(int x, int y);

protected:
    int m_maxX, m_maxY; // don't let it go off side of screen
};

#endif // STICK_H
