/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "Item.h"

Item::Item(sf::Image& image) {
    Initialize();
    SetImage(image);
}

void Item::Initialize() {
    m_coord.Setup(0.f, 0.f, 32.f, 32.f);
}

void Item::GenerateNewCoordinates() {
    int x = rand() % (m_maxX - 32);
    int y = rand() % (m_maxY - 32);

    m_coord.SetCoordinates((float) x, (float) y);
}

void Item::SetRange(int x, int y) {
    m_maxX = x;
    m_maxY = y;
}

