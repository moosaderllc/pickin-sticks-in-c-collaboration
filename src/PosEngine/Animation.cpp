/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "Animation.h"

namespace pe
{

    Animation::Animation()
    : m_maxFrames(0), m_maxAnims(0), m_delay(0.f), m_frame(0), m_anim(0) {
    }

    Animation::Animation(int frames, int animations, float delay) {
        m_frame = 0;
        m_anim = 0;
        Initialize(frames, animations, delay);
    }

    void Animation::Initialize(int frames, int animations, float delay) {
        m_maxFrames = frames;
        m_maxAnims = animations;
        m_delay = delay;
    }

    void Animation::Animate(sf::Sprite &s) {
        if (m_timer.GetElapsedTime() >= m_delay) {
            m_frame++;
            if (m_frame >= m_maxFrames) {
                m_frame = 0;
            }
            int x = (s.GetImage()->GetWidth() / m_maxFrames);
            int y = (s.GetImage()->GetHeight() / m_maxAnims);
            s.SetSubRect(sf::IntRect(m_frame*x, m_anim*y, m_frame * x + x, m_anim * y + y));
            m_timer.Reset();
        }
    }
}

