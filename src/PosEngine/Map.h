/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#ifndef MAP_H
#define MAP_H
#include "Tile.h"
#include "System.h"

namespace pe {
    namespace Map {

        /**
         * @author Luke Fangel, Joseph Montanez
         * @class Map
         * a class that renders a map
         **/
        class Map {
        public:
            Map();
            ~Map();
            void Initialize(sf::Image& mgr);

            void Render(System& sys);
            void Allocate(unsigned int l, unsigned int w, unsigned int h);
            void Deallocate();
        private:
            unsigned int m_width, m_height, m_layers;

            std::vector< std::vector< std::vector<Tile> > > m_tiles;
        };
    }
}
#endif // MAP_H
