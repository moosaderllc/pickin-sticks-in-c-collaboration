/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "Entity.h"

namespace pe
{

    Entity::Entity() {
        Initialize();
    }

    Entity::Entity(sf::Image& image) {
        Initialize();
        SetImage(image);
    }

    Entity::~Entity() {
    }

    void Entity::Initialize() {
        // void Setup( float x, float y, float w, float h )
        m_coord.Setup(0, 0, 32, 48);
    }

    /**
     * Entity::UpdateSprite
     * Makes sure the sprite is in the appropriate location
     */
    void Entity::UpdateSprite() {
        m_sprite.SetPosition(m_coord.x, m_coord.y);

    }

    /**
     * Entity::GetSprite
     * Updates and then returns the sprite for an external object to draw
     * @return sprite Sprite     Returns this entity's image sprite
     */
    sf::Sprite& Entity::GetSprite() {
        UpdateSprite();

        return m_sprite;
    }

    /**
     * Entity::SetImage
     * Wrapper for the entity object's Sprite object
     * @param image Image      Image object that this entity will be set to
     */
    void Entity::SetImage(sf::Image& image) {
        m_sprite.SetImage(image);
        m_sprite.SetSubRect(sf::IntRect(0, 0, 32, 48));
    }


    pe::Rectangle& Entity::GetRect() {
        return m_coord;
    }

    void Entity::SetRect( pe::Rectangle& rect )
    {
        m_coord = rect;
    }

    void Entity::SetCoords( int x, int y, int w, int h )
    {
        m_coord.x = x;
        m_coord.y = y;
        m_coord.w = w;
        m_coord.h = h;
    }

    bool Entity::CollisionWith( pe::Rectangle& objCoord )
    {
        return ( m_coord.x < objCoord.x + objCoord.w &&
                m_coord.x + m_coord.w > objCoord.x &&
                m_coord.y < objCoord.y + objCoord.h &&
                m_coord.y + m_coord.h > objCoord.y );
    }

    bool Entity::CollisionWith( Entity* obj )
    {
        return CollisionWith( obj->GetRect() );
    }
}
