/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "State.h"
#include "ManagerTemplate.h"
#include <vector>
#include <string>

#ifndef _StateManager
#define _StateManager

namespace pe
{
    /**
    * StateManager
    * Manager for game states
    * @author Rachel
    */
    class StateManager : public ManagerTemplate<StateInterface*>
    {
        public:
            void SetActiveState( std::string name );
            ProgramState MainLoop();
        private:
            StateInterface* m_activeState;
    };
}

#endif
