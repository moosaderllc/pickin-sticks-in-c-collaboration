/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#ifndef SOUNDWRAP_H_
#define SOUNDWRAP_H_

#include <iostream>

#include <SFML/Audio.hpp>

/**
 * SoundWrap: wraps a buffer and its identifier in a struct.
 * @author Terry Brashaw
 * @date Oct 5
 */

namespace pe
{
    class SoundWrap {
    public:
        // TODO: Members should be private
        sf::SoundBuffer m_sndbuff;
        sf::Sound m_snd;
        std::string m_name;
        int m_id;

        SoundWrap(const std::string& relPath, const std::string& m_name, int m_id, bool loops);
        void Setup(const std::string& relPath, const std::string& m_name, int m_id, bool loops);
    };
}

#endif
