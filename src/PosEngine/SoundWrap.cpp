/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

/**
 * SoundWrap methods.
 * @author Terry Brashaw
 * @date Oct 5
 */

#include "SoundWrap.h"

namespace pe
{

    /*
    SoundWrap::SoundWrap(const std::string& filename,const  std::string& identifier)
    {
            buffer.LoadFromFile(filename);
            this->identifier = identifier;
    }
     * */


    SoundWrap::SoundWrap(const std::string& relPath, const std::string& m_name, int iId, bool loops) {
        Setup(relPath, m_name, iId, loops);
    }

    /**
     * SoundWrap::Setup
     * Loads a file to the Sound item and sets up the IDs
     * @param relPath string		Path of the file
     * @param	m_name	string				String ID of the sound
     * @param iId int						Integer ID of the sound
     *
     * Refactored to match existing code better
     * @date Oct 18, 2010
     * @author Rachel
     * */
    void SoundWrap::Setup(const std::string& relPath, const std::string& m_name, int iID, bool loops) {
        if (!m_sndbuff.LoadFromFile(relPath)) {
            std::cerr << "Error loading in sound " << m_name << " at " << relPath << std::endl;
        } else {
            std::cout << "Loaded in sound " << relPath << " successfully!" << std::endl;

            m_snd.SetBuffer(m_sndbuff);
            m_snd.SetLoop(loops);
        }

        this->m_name = m_name;
        this->m_id = iID;
    }
}
