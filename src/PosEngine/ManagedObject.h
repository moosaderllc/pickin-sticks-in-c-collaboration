/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include <string>
#include <iostream>

#ifndef _ManagedObject
#define _ManagedObject

namespace pe
{
    class ManagedObject
    {
        public:
            ManagedObject() { m_name = "uninitialized"; }
            std::string m_name;

            std::string GetName()
            {
                std::cout<<"GetName"<<std::endl;
                std::cout<<(&m_name)<<std::endl;
                return m_name;
            }
        protected:
    };
}

#endif
