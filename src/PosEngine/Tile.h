/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#ifndef TILE_H
#define TILE_H
#include "Rectangle.h"
#include "ImageManager.h"

namespace pe {
    namespace Map {

        /**
         * @author Luke Fangel
         * @struct Tile
         * just a tile class
         **/
        struct Tile {
            pe::Rectangle rect;
            sf::Sprite image;
            int id;
        };
    }
}
#endif // TILE_H
