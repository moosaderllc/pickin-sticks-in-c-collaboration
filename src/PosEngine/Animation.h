/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#ifndef ANIMATION_H
#define ANIMATION_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>

/**
  @author Luke Fangel
  @class Animation

  the animation class will just hold the frame and animation of the character
  and increment it on animate if the time is right :D
 **/
namespace pe
{
    class Animation {
    private:
        sf::Clock m_timer;
        int m_maxFrames;
        int m_maxAnims;
        float m_delay;
        int m_frame;
        int m_anim;
    public:
        Animation();
        Animation(int frames, int animations, float delay);
        void Initialize(int frames, int animations, float delay);

        void SetMaxFrames(int f) {
            m_maxFrames = f;
        }

        void SetMaxAnimations(int f) {
            m_maxAnims = f;
        }

        void SetDelay(float delay) {
            m_delay = delay;
        }

        void ChangeAnim(int anim) {
            m_anim = anim;
        }

        //void Animate();
        void Animate(sf::Sprite& s);

    };

}

#endif // ANIMATION_H
