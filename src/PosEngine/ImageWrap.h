/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#ifndef _IMAGEWRAP
#define _IMAGEWRAP

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>

#include "ManagedObject.h"
/**
 * Wrapper for SFML's sf::Image class.  Contains a string and integer ID to help
 * ImageManager locate a specific image.
 */
namespace pe
{
    class ImageWrap : public ManagedObject
    {
        public:
            ImageWrap() { ; }
            ImageWrap(const std::string& relPath, const std::string& sId);
            ImageWrap(const sf::Image& image, const std::string& sId);

            void Setup(const std::string& relPath, const std::string& sId);
            void Setup(const sf::Image& image, const std::string& sId);

            sf::Image& GetImage() { return m_img; }
        private:
            sf::Image m_img;
    };
}

#endif
