/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include <string>
#include <map>
#include <iostream>

#ifndef _ManagerTemplate
#define _ManagerTemplate

namespace pe
{
    template <class ITEM>
    class ManagerTemplate
    {
        public:
            ITEM& GetAsset( const std::string& name )
            {
                return m_items[ name ];
            }

            void AddAsset( std::string name, ITEM item )
            {
                m_items.insert( std::pair<std::string, ITEM>( name, item ) );
            }

            int Size() { return m_items.size(); }

            // Overload []
            ITEM& operator[]( const std::string name )
            {
                return m_items[name];
            }

        protected:
            std::map< std::string, ITEM > m_items;
    };
}

#endif
