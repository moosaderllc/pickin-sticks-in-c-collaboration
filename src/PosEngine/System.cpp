/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "System.h"

namespace pe
{

    System::System() {
        Initialize();
        program.SetFramerateLimit(60);
    }

    System::~System() {
        program.Close();
    }

    /**
     * System::Initialize
     * Initialize the program and library to get it ready to start the program.
     */
    void System::Initialize() {
        m_screenWidth = 800;
        m_screenHeight = 600;
        m_bpp = 32;
        m_state = MENU;

        program.Create(sf::VideoMode(m_screenWidth, m_screenHeight, m_bpp), "Pickin' Open Sticks");
    }

    /**
     * System::SetState
     * Sets the "m_state" variable in the Program class.  Can be RUNNING, PAUSED, or EXIT.
     * @param val ProgramState     The new program m_state
     */
    void System::SetState(ProgramState val) {
        m_state = val;
    }

    /**
     * System::GetState()
     * Returns the current m_state of the program.  Can be RUNNING, PAUSED, or EXIT.
     * @return Returns the program's m_state
     */
    ProgramState System::GetState() {
        return m_state;
    }

    /**
     * System::BeginDraw
     * Wrapper to get the RenderWindow ready to draw stuff
     */
    void System::BeginDraw() {
        program.Clear(sf::Color(25, 125, 25));
    }

    /**
     * System::EndDraw
     * Wrapper to display contents to the screen
     */
    void System::EndDraw() {
        program.Display();
    }

    /**
     * System::Draw
     * Wrapper to draw a sprite to the buffer
     * @param sprite Sprite      The Sprite object to draw
     */
    void System::Draw(sf::Sprite &sprite) {
        program.Draw(sprite);
    }

    /**
     * System::CheckEvents()
     * Wrapper for RenderWindow's GetEvent function
     */
    void System::CheckEvents() {
        while (program.GetEvent(m_event)) {

            if (m_event.Type == sf::Event::Closed)
                m_state = EXIT;
            else
            {
                if (m_event.Type == sf::Event::KeyPressed)
                {
                    if (m_event.Key.Code == sf::Key::Escape)
                    {
                        (this->m_state == MENU) ? m_state = EXIT : m_state = MENU;
                    }
                }
            }
        }
    }

    void System::HandleKeyboardInput( Character& player ) {
        // Handle input - Player controls
        if (Key(sf::Key::Left) || Key(sf::Key::A)) {
            player.Move(LEFT);
        } else if (Key(sf::Key::Right) || Key(sf::Key::D)) {
            player.Move(RIGHT);
        }
        if (Key(sf::Key::Up) || Key(sf::Key::W)) {
            player.Move(UP);
        } else if (Key(sf::Key::Down) || Key(sf::Key::S)) {
            player.Move(DOWN);
        }
        if (Key(sf::Key::LShift)) {
            player.SetSpeed( 10.0F );
        }
        else {
            player.SetSpeed( 3.0F );
        }

        // Handle input - Program controls
        if (Key(sf::Key::F4)) {
            SetState(EXIT);
        }
    }

    /**
     * System::Key
     * Returns whether or not this key is being hit
     * @return code KeyCode
     */
    bool System::Key(sf::Key::Code code) {
        return program.GetInput().IsKeyDown(code);
    }

    /**
     * System::GetFramesPerSecond
     * Returns the frames per second
     * @return string The Frame Rate in a string format
     */
    std::string System::GetFramesPerSecond() {
        char fps[12];
        int fpsLength = sprintf(fps, "%.2f", 1.0f / program.GetFrameTime());
        return std::string(fps, fpsLength);
    }
}

