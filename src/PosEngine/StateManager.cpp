/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "StateManager.h"

namespace pe
{
    void StateManager::SetActiveState( std::string name )
    {
        m_activeState = GetAsset( name );
    }

    ProgramState StateManager::MainLoop()
    {
        return m_activeState->MainLoop();
    }
}

