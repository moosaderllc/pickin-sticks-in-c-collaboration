/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#ifndef _RECTANGLE
#define _RECTANGLE

#include <SFML/System.hpp>

/**
 * Rectangle
 * Wrapper for float values x, y, w and h for entity objects.
 */
/**
 * Edit: i had to make this in a namespace :D because of overwriting stuff
 */
namespace pe {
    class Rectangle {
    public:

        /*
         * @author luke fangel
         * @added constructors
         */
        Rectangle() : x(0), y(0), w(1), h(1) {
        }

        Rectangle(int _x, int _y, int _w, int _h)
        : x(_x), y(_y), w(_w), h(_h) {
        }

        Rectangle(sf::Vector2f xy, sf::Vector2f wh) {
            x = (int) xy.x;
            y = (int) xy.y;
            w = (int) wh.x;
            h = (int) wh.y;
        }



        int x, y, w, h;

        void Setup(int x, int y, int w, int h) {
            this->x = x;
            this->y = y;
            this->w = w;
            this->h = h;
        }

        void SetCoordinates(int x, int y) {
            this->x = x;
            this->y = y;
        }

        void SetDimensions(int w, int h) {
            this->w = w;
            this->h = h;
        }
    };
}


#endif
