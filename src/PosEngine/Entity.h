/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#ifndef _ENTITY
#define _ENTITY

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>

#include "ManagedObject.h"
#include "Rectangle.h"

namespace pe
{
    class Entity : public ManagedObject
    {
    protected:
        sf::Sprite m_sprite;
        pe::Rectangle m_coord; // coordinates & dimensions
    public:
        // Init/Deinit
        Entity();
        Entity(sf::Image& image);
        ~Entity();
        void Initialize();

        // Object functionality
        void UpdateSprite();

        // Get/set
        void SetImage(sf::Image& image);
        sf::Sprite &GetSprite();

        pe::Rectangle& GetRect();
        void SetRect( pe::Rectangle& rect );
        void SetCoords( int x, int y, int w, int h );

        bool CollisionWith( pe::Rectangle& objCoord );

        bool CollisionWith( Entity* obj );
    };
}

#endif

