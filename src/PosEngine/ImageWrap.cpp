/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "ImageWrap.h"

namespace pe
{

    ImageWrap::ImageWrap(const std::string& relPath, const std::string& sId)
    {
        Setup(relPath, sId);
    }

    void ImageWrap::Setup(const sf::Image &image, const std::string &sId)
    {
        m_name = sId;
        m_img = image;
        m_img.CreateMaskFromColor(sf::Color(255, 0, 255));
    }

    ImageWrap::ImageWrap(const sf::Image &image, const std::string &sId)
    {
        Setup(image, sId);
    }

    /**
     * ImageWrap::Setup
     * Loads a file to the Image item, and sets up the IDs
     * @param relPath string       Path of the file
     * @param sId string           String ID of the image
     */
    void ImageWrap::Setup(const std::string& relPath, const std::string& sId)
    {
        std::cout << "Loading in image " << relPath << "...";
        if (!m_img.LoadFromFile(relPath))
        {
            std::cerr << "Error loading in image " << sId << " at " << relPath << std::endl;
        }
        else
        {
            std::cout << "Success!" << std::endl;
        }

        m_img.CreateMaskFromColor(sf::Color(255, 0, 255));

        /* Turn Smoothing Off, or else tiles will have lines between them */
        m_img.SetSmooth(false);

        m_name = sId;
    }
}
