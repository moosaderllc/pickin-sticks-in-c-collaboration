/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#ifndef _IMAGEMANAGER
#define _IMAGEMANAGER

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>

#include "ManagerTemplate.h"
#include "ImageWrap.h"

/**
 * ImageManager
 * Contains all the images in the game
 */
namespace pe
{
    class ImageManager : public pe::ManagerTemplate<ImageWrap>
    {
        public:
            void Init();
    };
}

#endif
