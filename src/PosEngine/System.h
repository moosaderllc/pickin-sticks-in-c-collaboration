/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#ifndef _SYSTEM
#define _SYSTEM

#include "../Include.h" // TODO: Remove
#include <stdio.h>
#include "../Character.h"

class Character;

enum ProgramState {
    MENU, RUNNING, PAUSED, EXIT
};

/**
 * System class-
 * Handles library and program initialization, basic window sizing.
 * @date 2010-10-02
 * @author Rachel
 */

namespace pe
{
    class System {
    private:
        int m_screenWidth, m_screenHeight, m_bpp;
        bool m_isFullscreen;
        ProgramState m_state;
        sf::Event m_event;
    public:
        sf::RenderWindow program;

        // Init/Deinit
        System();
        ~System();
        void Initialize();

        // Object functionality
        void BeginDraw();
        void EndDraw();
        void Draw(sf::Sprite &sprite);
        void CheckEvents();
        void HandleKeyboardInput( Character& player);
        bool Key(sf::Key::Code code);

        // Get/Set functions
        std::string GetFramesPerSecond();

        void SetState(ProgramState val);
        ProgramState GetState();

        int GetScreenWidth() {
            return m_screenWidth;
        }

        int GetScreenHeight() {
            return m_screenHeight;
        }
    };
}

#endif

