/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "ImageManager.h"

namespace pe
{

    /**
     * ImageManager::Initialize
     * Allocates memory for the ImageWrap array and loads in all the image files.
     */
    void ImageManager::Init()
    {
        // TODO: This does not belong in the engine code.
        std::string name = "default";
        ImageWrap imgDefault("assets/gfx/default.png",      name);
        AddAsset( name, imgDefault );

        name = "stick";
        ImageWrap imgStick("assets/gfx/stick.png",          name);
        AddAsset( name, imgStick );

        name = "player";
        ImageWrap imgPlayer("assets/gfx/delphine.png",      name);
        AddAsset( name, imgPlayer );

        name = "grass";
        ImageWrap imgGrass("assets/gfx/fluffy-grass.png",   name);
        AddAsset( name, imgGrass );
    }
}
