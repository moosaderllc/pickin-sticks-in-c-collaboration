/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#ifndef SOUNDMANAGER_H_
#define SOUNDMANAGER_H_

#include <SFML/System.hpp>

#include "SoundWrap.h"

/**
 * SoundManager: holds the sf::sf::SoundBuffer data.
 * @author Terry Brashaw
 * @date Oct 5
 */
namespace pe
{
    class SoundManager {
    private:
        std::vector<SoundWrap*> m_soundWrap;
        int FindIndex(const std::string& sId);

    public:
        // Init/Deinit
        SoundManager();
        ~SoundManager();
        void Initialize();

        // Object functionality
        void AddSound(const std::string& filename, const std::string& identifier, bool loops);
        void PlaySound(const std::string& sId);
        void PlaySound(int iId);

        // Get/Set
        sf::SoundBuffer& GetSoundBuffer(const std::string& sId);
        sf::SoundBuffer& GetSoundBuffer(int iId);
    };
}

#endif
