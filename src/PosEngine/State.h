/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include <string>
#include "System.h"
#include "ManagedObject.h"

#ifndef STATE_H
#define STATE_H

namespace pe
{
    class StateInterface : public ManagedObject
    {
        public:
            StateInterface();
            StateInterface( std::string name, System* system );

            virtual void Init() = 0;
            virtual ProgramState MainLoop() = 0;
            void SetSystem( System* system ) { m_system = system; }

        protected:
            bool m_closeState;
            bool m_pause;

            System* m_system;
    };
}

#endif
