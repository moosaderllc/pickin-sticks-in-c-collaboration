/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "Map.h"

namespace pe {
    namespace Map {

        Map::Map()
        : m_width(0), m_height(0), m_layers(0) {
        }

        Map::~Map() {
            this->Deallocate();
        }

        void Map::Allocate(unsigned int l, unsigned int w, unsigned int h) {
            m_layers = l;
            m_width = w;
            m_height = h;
        }

        void Map::Deallocate() {
            // Do I need to clear anything?
        }

        void Map::Initialize(sf::Image& img) {
            Allocate(1, 25, 19);

            std::vector< std::vector<Tile> > layer;
            unsigned int x, y;
            for (x = 0; x < m_width; x++) {
                std::vector<Tile> row;
                for (y = 0; y < m_height; y++) {
                    Tile t;

                    t.image.SetImage(img);
                    t.image.SetPosition(x * 32, y * 32);
                    t.rect.Setup(x * 32, y * 32, 32, 32);

                    row.push_back(t);
                }
                layer.push_back(row);
            }
            m_tiles.push_back(layer);
        }

        void Map::Render(System &sys) {
            unsigned int l, x, y;
            for (l = 0; l < m_tiles.size(); l++) {
                for (x = 0; x < m_tiles[l].size(); x++) {
                    for (y = 0; y < m_tiles[l][x].size(); y++) {
                        sys.Draw(m_tiles[l][x][y].image);
                    }
                }
            }
        }
    }
}
