/*
Pickin' Open Sticks
Homepage:   http://code.google.com/p/pickin-open-sticks/
License:    GNU GPL v3 http://www.gnu.org/licenses/gpl.html
*/

#include "State.h"

namespace pe
{

    StateInterface::StateInterface()
    {
    }

    StateInterface::StateInterface( std::string name, System* system )
    {
        SetSystem( system );
        m_name = name;
    }

}
